<script>
    (function($){
        $(window).load(function(){
            $(".table-responsive").mCustomScrollbar({
                axis:"yx",
                theme:"dark"
        });
        });
    })(jQuery);
</script>
<div class="row">
<div class="col-md-12">
<div class="row">
<div class="col-md-12">
<button class="btn btn-info" data-toggle="collapse" data-target="#wyszukiwanie_zaawansowane">Wyszukiwanie</button>
</div>
</div>
<br>
<div class="row">
<div id="wyszukiwanie_zaawansowane" class="collapse col-md-12">
<?php $search = '';echo $this->Form->create($search);?>
<fieldset>
<div class="row">
<div class="col-md-6">
        <?php
            echo $this->Form->input('nip', array('class' => 'form-control', 'label' => 'NIP'));
        ?>
</div>
<div class="col-md-6">

        <?php
            echo $this->Form->input('regon', array('class' => 'form-control', 'label' => 'REGON'));
        ?>

</div>
</div>

<div class="row">
<div class="col-md-6">

        <?php
            echo $this->Form->input('name', array('class' => 'form-control', 'label' => 'Nazwa firmy'));
        ?>

</div>
</div>

<div class="row">
<div class="col-md-6">

        <?php
            echo $this->Form->input('first_name', array('class' => 'form-control', 'label' => 'Imię'));
        ?>

</div>
<div class="col-md-6">

        <?php
            echo $this->Form->input('last_name', array('class' => 'form-control', 'label' => 'Nazwisko'));
        ?>

</div>
</div>

<div class="row">
<div class="col-md-6">

        <?php
            echo $this->Form->input('main_adr_region', array('class' => 'form-control', 'label' => 'Województwo'));
        ?>

</div>
<div class="col-md-6">

        <?php
            echo $this->Form->input('main_adr_district', array('class' => 'form-control', 'label' => 'Powiat'));
        ?>

</div>
</div>

<div class="row">
<div class="col-md-6">

        <?php
            echo $this->Form->input('main_adr_city', array('class' => 'form-control', 'label' => 'Miasto'));
        ?>

</div>
<div class="col-md-6">

        <?php
            echo $this->Form->input('post_adr_city', array('class' => 'form-control', 'label' => 'Gmina'));
        ?>

</div>
</div>

<div class="row">
<div class="col-md-6">

        <?php
            echo $this->Form->input('main_adr_street', array('class' => 'form-control', 'label' => 'Ulica'));
        ?>

</div>
<div class="col-md-6">

        <?php
            echo $this->Form->input('main_pkd', array('class' => 'form-control', 'label' => 'Rodzaj działalności (PKD)'));
        ?>

</div>
</div>
</fieldset>
<?php echo $this->Form->submit('Wyszukaj', ['class' => 'btn btn-info']);?>
<?= $this->Form->end() ?>
</div>
</div>
</div>
</div>
<div class="row">
<div class="table-responsive col-md-12" style="height: 400px; !important;">
<table class="table table-hover">
            <thead>
            <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('first_name') ?></th>
                    <th><?= $this->Paginator->sort('last_name') ?></th>
                    <th><?= $this->Paginator->sort('regon') ?></th>
                    <th><?= $this->Paginator->sort('nip') ?></th>
                    <th><?= $this->Paginator->sort('www') ?></th>
                    <th><?= $this->Paginator->sort('email') ?></th>
                    <th><?= $this->Paginator->sort('started') ?></th>
                    <th><?= $this->Paginator->sort('main_pkd') ?></th>
                    <th><?= $this->Paginator->sort('main_adr_street') ?></th>
                    <th><?= $this->Paginator->sort('main_adr_code') ?></th>
                    <th><?= $this->Paginator->sort('main_adr_city') ?></th>
                    <th><?= $this->Paginator->sort('main_adr_region') ?></th>
                    <th><?= $this->Paginator->sort('main_adr_district') ?></th>
                    <th><?= $this->Paginator->sort('post_adr_code') ?></th>
                    <th><?= $this->Paginator->sort('post_adr_city') ?></th>
                    <th><?= $this->Paginator->sort('post_adr_region') ?></th>
                    <th><?= $this->Paginator->sort('post_adr_district') ?></th>
                    <th><?= $this->Paginator->sort('post_adr_street') ?></th>
                    <th><?= $this->Paginator->sort('source') ?></th>
                    <th><?= $this->Paginator->sort('status') ?></th>
                    <th><?= $this->Paginator->sort('addi_2') ?></th>
                    <th><?= $this->Paginator->sort('addi_4') ?></th>
                    <th><?= $this->Paginator->sort('addi_5') ?></th>
                    <th><?= $this->Paginator->sort('addi_6') ?></th>
                    <th><?= $this->Paginator->sort('addi_7') ?></th>
                    <th><?= $this->Paginator->sort('url') ?></th>
                    <th><?= $this->Paginator->sort('other_pkd') ?></th>
                    <th><?= $this->Paginator->sort('quality') ?></th>
                    <th><?= $this->Paginator->sort('street') ?></th>
                    <th><?= $this->Paginator->sort('street_id') ?></th>
                    <th><?= $this->Paginator->sort('city_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($firms as $firm): ?>
                    <tr>
                        <td><?= $this->Number->format($firm->id) ?></td>
                        <td><?= h($firm->name) ?></td>
                        <td><?= h($firm->first_name) ?></td>
                        <td><?= h($firm->last_name) ?></td>
                        <td><?= $this->Number->format($firm->regon) ?></td>
                        <td><?= $this->Number->format($firm->nip) ?></td>
                        <td><?= h($firm->www) ?></td>
                        <td><?= h($firm->email) ?></td>
                        <td><?= h($firm->started) ?></td>
                        <td><?= h($firm->main_pkd) ?></td>
                        <td><?= h($firm->main_adr_street) ?></td>
                        <td><?= h($firm->main_adr_code) ?></td>
                        <td><?= h($firm->main_adr_city) ?></td>
                        <td><?= h($firm->main_adr_region) ?></td>
                        <td><?= h($firm->main_adr_district) ?></td>
                        <td><?= h($firm->post_adr_code) ?></td>
                        <td><?= h($firm->post_adr_city) ?></td>
                        <td><?= h($firm->post_adr_region) ?></td>
                        <td><?= h($firm->post_adr_district) ?></td>
                        <td><?= h($firm->post_adr_street) ?></td>
                        <td><?= h($firm->source) ?></td>
                        <td><?= h($firm->status) ?></td>
                        <td><?= h($firm->addi_2) ?></td>
                        <td><?= h($firm->addi_4) ?></td>
                        <td><?= h($firm->addi_5) ?></td>
                        <td><?= h($firm->addi_6) ?></td>
                        <td><?= h($firm->addi_7) ?></td>
                        <td><?= h($firm->url) ?></td>
                        <td><?= h($firm->other_pkd) ?></td>
                        <td><?= h($firm->quality) ?></td>
                        <td><?= h($firm->street) ?></td>
                        <td><?= $firm->has('street') ? $this->Html->link($firm->street->name, ['controller' => 'Streets', 'action' => 'view', $firm->street->id]) : '' ?></td>
                        <td><?= $firm->has('city') ? $this->Html->link($firm->city->name, ['controller' => 'Cities', 'action' => 'view', $firm->city->id]) : '' ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('View'), ['action' => 'view', $firm->id]) ?>
                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $firm->id]) ?>
                            <?= $this->Html->link(__('Create PDF'), ['action' => 'pdf', $firm->id, $wyszukaj]) ?>
                            <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $firm->id], ['confirm' => __('Jesteś pewien, że chcesz usunąć tą firmę? # {0}?', $firm->id)]) ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
</table>
</div>
<div class="paginator col-md-12">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('wstecz')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('następna') . ' >') ?>
        </ul>
        <p class="lead"><?= $this->Paginator->counter() ?></p>
</div>      
</div>