<div class="col-md-9">
    <h3><?= h($firm->name) ?></h3>
    <table class="table table-stripped">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($firm->name) ?></td>
        </tr>
        <tr>
            <th><?= __('First Name') ?></th>
            <td><?= h($firm->first_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Last Name') ?></th>
            <td><?= h($firm->last_name) ?></td>
        </tr>
        <tr>
            <th><?= __('Www') ?></th>
            <td><?= h($firm->www) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($firm->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Main Pkd') ?></th>
            <td><?= h($firm->main_pkd) ?></td>
        </tr>
        <tr>
            <th><?= __('Main Adr Street') ?></th>
            <td><?= h($firm->main_adr_street) ?></td>
        </tr>
        <tr>
            <th><?= __('Main Adr Code') ?></th>
            <td><?= h($firm->main_adr_code) ?></td>
        </tr>
        <tr>
            <th><?= __('Main Adr City') ?></th>
            <td><?= h($firm->main_adr_city) ?></td>
        </tr>
        <tr>
            <th><?= __('Main Adr Region') ?></th>
            <td><?= h($firm->main_adr_region) ?></td>
        </tr>
        <tr>
            <th><?= __('Main Adr District') ?></th>
            <td><?= h($firm->main_adr_district) ?></td>
        </tr>
        <tr>
            <th><?= __('Post Adr Code') ?></th>
            <td><?= h($firm->post_adr_code) ?></td>
        </tr>
        <tr>
            <th><?= __('Post Adr City') ?></th>
            <td><?= h($firm->post_adr_city) ?></td>
        </tr>
        <tr>
            <th><?= __('Post Adr Region') ?></th>
            <td><?= h($firm->post_adr_region) ?></td>
        </tr>
        <tr>
            <th><?= __('Post Adr District') ?></th>
            <td><?= h($firm->post_adr_district) ?></td>
        </tr>
        <tr>
            <th><?= __('Post Adr Street') ?></th>
            <td><?= h($firm->post_adr_street) ?></td>
        </tr>
        <tr>
            <th><?= __('Source') ?></th>
            <td><?= h($firm->source) ?></td>
        </tr>
        <tr>
            <th><?= __('Status') ?></th>
            <td><?= h($firm->status) ?></td>
        </tr>
        <tr>
            <th><?= __('Addi 2') ?></th>
            <td><?= h($firm->addi_2) ?></td>
        </tr>
        <tr>
            <th><?= __('Addi 4') ?></th>
            <td><?= h($firm->addi_4) ?></td>
        </tr>
        <tr>
            <th><?= __('Addi 5') ?></th>
            <td><?= h($firm->addi_5) ?></td>
        </tr>
        <tr>
            <th><?= __('Addi 6') ?></th>
            <td><?= h($firm->addi_6) ?></td>
        </tr>
        <tr>
            <th><?= __('Addi 7') ?></th>
            <td><?= h($firm->addi_7) ?></td>
        </tr>
        <tr>
            <th><?= __('Url') ?></th>
            <td><?= h($firm->url) ?></td>
        </tr>
        <tr>
            <th><?= __('Other Pkd') ?></th>
            <td><?= h($firm->other_pkd) ?></td>
        </tr>
        <tr>
            <th><?= __('Quality') ?></th>
            <td><?= h($firm->quality) ?></td>
        </tr>
        <tr>
            <th><?= __('Street') ?></th>
            <td><?= h($firm->street) ?></td>
        </tr>
        <tr>
            <th><?= __('Street') ?></th>
            <td><?= $firm->has('street') ? $this->Html->link($firm->street->name, ['controller' => 'Streets', 'action' => 'view', $firm->street->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('City') ?></th>
            <td><?= $firm->has('city') ? $this->Html->link($firm->city->name, ['controller' => 'Cities', 'action' => 'view', $firm->city->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($firm->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Regon') ?></th>
            <td><?= $this->Number->format($firm->regon) ?></td>
        </tr>
        <tr>
            <th><?= __('Nip') ?></th>
            <td><?= $this->Number->format($firm->nip) ?></td>
        </tr>
        <tr>
            <th><?= __('Started') ?></th>
            <td><?= h($firm->started) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Pkds') ?></h4>
        <?php if (!empty($firm->pkds)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Symbol') ?></th>
                <th><?= __('Section') ?></th>
                <th><?= __('Column') ?></th>
                <th><?= __('Group') ?></th>
                <th><?= __('Class') ?></th>
                <th><?= __('Description') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($firm->pkds as $pkds): ?>
            <tr>
                <td><?= h($pkds->id) ?></td>
                <td><?= h($pkds->symbol) ?></td>
                <td><?= h($pkds->section) ?></td>
                <td><?= h($pkds->column) ?></td>
                <td><?= h($pkds->group) ?></td>
                <td><?= h($pkds->class) ?></td>
                <td><?= h($pkds->description) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Pkds', 'action' => 'view', $pkds->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Pkds', 'action' => 'edit', $pkds->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Pkds', 'action' => 'delete', $pkds->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pkds->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
