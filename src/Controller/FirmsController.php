<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Firms Controller
 *
 * @property \App\Model\Table\FirmsTable $Firms
 */
class FirmsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function initialize()
    {
    parent::initialize();
    $this->loadComponent('Search.Prg', [
        'actions' => ['index']
    ]);
    }

    public function index()
    {
        //Zapytanie z wyszukiwania
        
        $contitions = array();
        
        foreach($this->request->query as $key => $value){
            if($this->Firms->hasField($key)){
            $conditions["firms.".$key." Like"] = "%".$value."%";
            }
        }
        if(isset($conditions)){
            $query = $this->Firms->find()->where($conditions);
        } else {
            $query = $this->Firms;
        }
        
        $this->set('wyszukaj', $this->request->query('Wyszukaj'));
        $this->set('firms', $this->paginate($query));
    }
    
    public function pdf($id = null, $wyszukaj = null){
        $this->pdfConfig = array(
            'orientation' => 'landscape',
            'pageSize' => 'A4'
        );
        $CakePdf = new \CakePdf\Pdf\CakePdf();
        $CakePdf->template('view', 'default');
        $firm = $this->Firms->get($id);
        $CakePdf->viewVars(array('firm'=>$firm));
        // Get the PDF string returned
        $pdf = $CakePdf->output();
        // Or write it to file directly
        $pdf = $CakePdf->write(APP . 'files' . DS .'envelopes'. DS. $firm->name .'.pdf');
        $this->redirect(array("controller" => "firms", 'Wyszukaj' => $wyszukaj));
    }

    /**
     * View method
     *
     * @param string|null $id Firm id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $firm = $this->Firms->get($id, [
            'contain' => []
        ]);

        $this->set('firm', $firm);
        $this->set('_serialize', ['firm']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $firm = $this->Firms->newEntity();
        if ($this->request->is('post')) {
            $firm = $this->Firms->patchEntity($firm, $this->request->data);
            if ($this->Firms->save($firm)) {
                $this->Flash->success(__('The firm has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The firm could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('firm'));
        $this->set('_serialize', ['firm']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Firm id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $firm = $this->Firms->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $firm = $this->Firms->patchEntity($firm, $this->request->data);
            if ($this->Firms->save($firm)) {
                $this->Flash->success(__('The firm has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The firm could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('firm'));
        $this->set('_serialize', ['firm']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Firm id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $firm = $this->Firms->get($id);
        if ($this->Firms->delete($firm)) {
            $this->Flash->success(__('The firm has been deleted.'));
        } else {
            $this->Flash->error(__('The firm could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
