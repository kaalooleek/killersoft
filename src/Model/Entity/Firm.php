<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Firm Entity.
 *
 * @property int $id
 * @property string $name
 * @property string $first_name
 * @property string $last_name
 * @property int $regon
 * @property int $nip
 * @property string $www
 * @property string $email
 * @property \Cake\I18n\Time $started
 * @property string $main_pkd
 * @property string $main_adr_street
 * @property string $main_adr_code
 * @property string $main_adr_city
 * @property string $main_adr_region
 * @property string $main_adr_district
 * @property string $post_adr_code
 * @property string $post_adr_city
 * @property string $post_adr_region
 * @property string $post_adr_district
 * @property string $post_adr_street
 * @property string $source
 * @property string $status
 * @property string $addi_2
 * @property string $addi_4
 * @property string $addi_5
 * @property string $addi_6
 * @property string $addi_7
 * @property string $url
 * @property string $other_pkd
 * @property string $quality
 * @property \App\Model\Entity\Street $street
 * @property int $street_id
 * @property int $city_id
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\Pkd[] $pkds
 */
class Firm extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
