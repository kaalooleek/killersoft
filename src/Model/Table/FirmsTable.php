<?php
namespace App\Model\Table;

use App\Model\Entity\Firm;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Search\Manager;

/**
 * Firms Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Streets
 * @property \Cake\ORM\Association\BelongsTo $Cities
 * @property \Cake\ORM\Association\BelongsToMany $Pkds
 */
class FirmsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('id', 'Search.Value')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
            ->add('Wyszukaj', 'Search.Like', [
                'before' => true,
                'after' => true,
                'field' => [$this->aliasField('name'), $this->aliasField('first_name'), $this->aliasField('last_name'), $this->aliasField('nip'), $this->aliasField('regon')]
            ])
            ->add('foo', 'Search.Callback', [
                'callback' => function ($query, $args, $manager) {
                    // Modify $query as required
                }
            ]);
        $this->table('firms');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsTo('Streets', [
            'foreignKey' => 'street_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Requests', [
            'foreignKey' => 'request_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Districts', [
            'foreignKey' => 'district_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Regions', [
            'foreignKey' => 'region_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Pkds', [
            'foreignKey' => 'firm_id',
            'targetForeignKey' => 'pkd_id',
            'joinTable' => 'pkds_firms'
        ]);
        $this->belongsToMany('Subcamps', [
            'foreignKey' => 'firm_id',
            'targetForeignKey' => 'subcamp_id',
            'joinTable' => 'subcamps_firms'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->requirePresence('regon', 'create')
            ->notEmpty('regon');

        $validator
            ->requirePresence('nip', 'create')
            ->notEmpty('nip');

        $validator
            ->requirePresence('www', 'create')
            ->notEmpty('www');

        $validator
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->date('started')
            ->requirePresence('started', 'create')
            ->notEmpty('started');

        $validator
            ->requirePresence('main_pkd', 'create')
            ->notEmpty('main_pkd');

        $validator
            ->requirePresence('main_adr_street', 'create')
            ->notEmpty('main_adr_street');

        $validator
            ->requirePresence('main_adr_code', 'create')
            ->notEmpty('main_adr_code');

        $validator
            ->requirePresence('main_adr_city', 'create')
            ->notEmpty('main_adr_city');

        $validator
            ->requirePresence('main_adr_region', 'create')
            ->notEmpty('main_adr_region');

        $validator
            ->requirePresence('main_adr_district', 'create')
            ->notEmpty('main_adr_district');

        $validator
            ->requirePresence('post_adr_code', 'create')
            ->notEmpty('post_adr_code');

        $validator
            ->requirePresence('post_adr_city', 'create')
            ->notEmpty('post_adr_city');

        $validator
            ->requirePresence('post_adr_region', 'create')
            ->notEmpty('post_adr_region');

        $validator
            ->requirePresence('post_adr_district', 'create')
            ->notEmpty('post_adr_district');

        $validator
            ->requirePresence('post_adr_street', 'create')
            ->notEmpty('post_adr_street');

        $validator
            ->requirePresence('source', 'create')
            ->notEmpty('source');

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        $validator
            ->requirePresence('addi_2', 'create')
            ->notEmpty('addi_2');

        $validator
            ->requirePresence('addi_4', 'create')
            ->notEmpty('addi_4');

        $validator
            ->requirePresence('addi_5', 'create')
            ->notEmpty('addi_5');

        $validator
            ->requirePresence('addi_6', 'create')
            ->notEmpty('addi_6');

        $validator
            ->requirePresence('addi_7', 'create')
            ->notEmpty('addi_7');

        $validator
            ->requirePresence('url', 'create')
            ->notEmpty('url');

        $validator
            ->requirePresence('other_pkd', 'create')
            ->notEmpty('other_pkd');

        $validator
            ->requirePresence('quality', 'create')
            ->notEmpty('quality');

        $validator
            ->notEmpty('street');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['street_id'], 'Streets'));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
        return $rules;
    }
}
